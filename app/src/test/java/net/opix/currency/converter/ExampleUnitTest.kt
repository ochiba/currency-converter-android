package net.opix.currency.converter

import org.junit.Test

import org.junit.Assert.*
import net.opix.currency.converter.models.Rate

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        val aud = Rate("AUD", 1.50)
        val result = 20 * aud.rate
        assertTrue(result == 30.0)
    }
}