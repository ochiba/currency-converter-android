package net.opix.currency.converter

import android.content.Context
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import com.google.android.material.snackbar.Snackbar
import net.opix.currency.converter.databinding.ActivityMainBinding
import net.opix.currency.converter.models.AllRatesResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import net.opix.currency.converter.models.Rate
import net.opix.currency.converter.models.DatabaseManager
import net.opix.currency.converter.service.ServiceBuilder
import net.opix.currency.converter.service.ServiceEndpoint

val sourceKey = "SourceKey"
val lastTimeDownloadedKey = "lastTimeDownloadedKey"

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var converter: Converter
    private lateinit var sharedPref: SharedPreferences
    private var sourceCurrency = "USD"
    private lateinit var manager: DatabaseManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        manager = DatabaseManager(this)
        converter = Converter
        sharedPref = applicationContext.getSharedPreferences(
            MainActivity::class.java.simpleName,
            Context.MODE_PRIVATE
        )

        setListeners()

        if (isWithin30Minutes()) {
            loadLastExchangeRates()

            Handler(Looper.getMainLooper()).postDelayed({
                // Show picker - Not the way I would like, but...
                binding.spCurrency.performClick()
            }, 2000)
        } else
            downloadExchangeRates()
    }

    private fun isWithin30Minutes(): Boolean {
        var lastTime = sharedPref.getInt(lastTimeDownloadedKey, 0)

        if (lastTime == 0)
            return false

        lastTime += 60 * 30 // 30 minutes
        val now = System.currentTimeMillis() / 1000
        return now < lastTime
    }

    private fun setListeners() {
        binding.sourceRate.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
                if (s.isEmpty())
                    binding.selectedRate.text.clear()
                else {
                    val amount = s.toString().toDouble()

                    if (amount == 0.0) {
                        binding.selectedRate.text.clear()
                    } else {
                        Handler(Looper.getMainLooper()).postDelayed({
                            binding.selectedRate.setText(converter.calculateToString(amount))
                        }, 500)
                    }
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })

        binding.spCurrency.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val newCurrency = converter.rates[position]

                if (converter.currentRate != newCurrency) {
                    converter.currentRate = newCurrency

                    val newEquation = "1 ${sourceCurrency} = ${newCurrency.rate} ${newCurrency.currency}"
                    binding.equation.setText(newEquation)

                    binding.sourceRate.text?.let {
                        val amount = it.toString().toDoubleOrNull()

                        if (amount != null)
                            binding.selectedRate.setText(converter.calculateToString(amount))
                    }

                    binding.sourceRate.requestFocus()
                }
            }
        }
    }

    private fun loadLastExchangeRates() {
        val rates = manager.getRates()

        converter.rates = rates.toTypedArray()
        binding.spCurrency.adapter = resetCurrencyAdapter()
    }

    private fun downloadExchangeRates() {
        val request = ServiceBuilder.buildService(ServiceEndpoint::class.java)
        val call = request.getAllRates(BuildConfig.API_KEY)

        call.enqueue(object : Callback<AllRatesResponse> {
            override fun onResponse(
                call: Call<AllRatesResponse>,
                response: Response<AllRatesResponse>
            ) {
                if (response.isSuccessful) {
                    val rates = ArrayList<Rate>()

                    Log.d("successful!", "${response}")

                    response.body()?.let { body ->
                        manager.deleteAllRates()

                        body.quotes.forEach { (key, value) ->
                            val sanitizedKey = key.removeRange(0, 3)
                            val rate = Rate(sanitizedKey, value)

                            rates.add(rate)
                            manager.add1Rate(sanitizedKey, value)
                        }
                        converter.rates = rates.toTypedArray()
                        binding.spCurrency.adapter = resetCurrencyAdapter()

                        // Show picker
                        binding.spCurrency.performClick()

                        val editor = sharedPref.edit()
                        body.source?.let {
                            editor.putString(sourceKey, it)

                            binding.sourceCurrency.setText(it)
                            sourceCurrency = it
                        }

                        editor.putInt(lastTimeDownloadedKey, body.timestamp)
                        editor.apply()
                    }
                }
            }

            override fun onFailure(call: Call<AllRatesResponse>, t: Throwable) {
                t.message?.let {
                    val snackBar = Snackbar.make(binding.root, it, Snackbar.LENGTH_LONG)
                    // Change the max line - 2 in case it's long.
                    val sbView = snackBar.view
                    val textView =
                        sbView.findViewById<TextView>(com.google.android.material.R.id.snackbar_text)
                    textView.setMaxLines(2)
                    snackBar.show()
                }
            }
        })
    }

    private fun resetCurrencyAdapter(): ArrayAdapter<String> {
        val list = converter.rates.map { "${it.currency} (${it.rate})" }
        val spinnerArrayAdapter = CurrencyAdapter(this@MainActivity as Context, list)

        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner)
        binding.spCurrency.isEnabled = converter.rates.size > 1
        return spinnerArrayAdapter
    }
}