package net.opix.currency.converter.models

import java.io.Serializable

data class AllRatesResponse(val success: Boolean, val terms: String?, val privacy: String?, val timestamp: Int, val source: String?, val quotes: Map<String, Double>) : Serializable {
}