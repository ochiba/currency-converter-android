package net.opix.currency.converter.service

import net.opix.currency.converter.models.AllRatesResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ServiceEndpoint {
    @GET("live&amp;format=1")
    fun getAllRates(@Query("access_key") key: String): Call<AllRatesResponse>
}