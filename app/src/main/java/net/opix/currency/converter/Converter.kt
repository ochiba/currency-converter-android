package net.opix.currency.converter

import android.icu.text.NumberFormat
import net.opix.currency.converter.models.Rate

object Converter {
    var currentRate: Rate? = null
    var rates: Array<Rate> = emptyArray()

    fun calculate(amount: Double): Double {
        if (currentRate == null) {
            return 0.0
        }

        return currentRate!!.rate * amount
    }

    fun calculateToString(amount: Double): String {
        val value = calculate(amount)

        if (value == 0.0 || currentRate == null) {
            return "N/A"
        }

        val format: NumberFormat = NumberFormat.getNumberInstance()
        format.setMaximumFractionDigits(2)
        return format.format(value)
    }
}