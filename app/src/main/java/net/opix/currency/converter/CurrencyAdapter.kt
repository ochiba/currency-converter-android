package net.opix.currency.converter

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView

open class CurrencyAdapter(context: Context, val list: List<String>) : ArrayAdapter<String>(context, R.layout.spinner, list) {
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        // May not be needed, but just in case...
        val index = if (position < 0 || position >= list.size) 0 else position
        return super.getView(index, convertView, parent) as TextView
    }

    override fun getCount(): Int {
        return list.size
    }

    override fun getItem(position: Int): String {
        return list[position]
    }
}