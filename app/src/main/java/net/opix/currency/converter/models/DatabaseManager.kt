package net.opix.currency.converter.models

import android.annotation.SuppressLint
import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteException
import android.database.sqlite.SQLiteOpenHelper
import android.widget.Toast
import java.util.*

class DatabaseManager(val context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {
    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL("CREATE TABLE IF NOT EXISTS [Rate] ([currency] TEXT DEFAULT '', [rate] REAL DEFAULT 0);")
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        // db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db)
    }

    override fun onDowngrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        onUpgrade(db, oldVersion, newVersion)
    }

    companion object {
        // If you change the database schema, you must increment the database version.
        val DATABASE_VERSION = 1
        val DATABASE_NAME = "currencyconverter.db"
    }

    @SuppressLint("Recycle")
    fun getRates(): ArrayList<Rate> {
        val rateList = ArrayList<Rate>()

        try {
            val db = readableDatabase
            val sql = "SELECT currency, rate FROM Rate ORDER BY currency"
            val cursor = db.rawQuery(sql, null)

            cursor.moveToFirst()

            while (!cursor.isAfterLast) {
               val oneRate = Rate(cursor.getString(0), cursor.getDouble(1))
                rateList.add(oneRate)
                cursor.moveToNext()
            }
            db.close()
        } catch (e: Exception) {
            Toast.makeText(context, e.localizedMessage, Toast.LENGTH_LONG).show()
        }

        return rateList
    }

    fun deleteAllRates(): Boolean {
        try {

            val db = writableDatabase
            db.beginTransaction()
            db.delete("Rate", null, null)
            db.setTransactionSuccessful()
            db.endTransaction()
            db.close()
        } catch (e: SQLiteException) {
            Toast.makeText(context, e.localizedMessage, Toast.LENGTH_LONG).show()
            return false
        }
        return true
    }

    fun add1Rate(currency: String, rate: Double): Boolean {

        val db = writableDatabase

        // Create a new map of values, where column names are the keys
        val values = ContentValues()
        values.put("currency", currency)
        values.put("rate", rate)

        try {
            db.beginTransaction()
            db.insertOrThrow(
                    "Rate", null,
                    values)
            db.setTransactionSuccessful()
            db.endTransaction()
            db.close()
        } catch (e: SQLiteException) {
            Toast.makeText(context, e.localizedMessage, Toast.LENGTH_LONG).show()
            db.close()
            return false
        }
        return true
    }
}