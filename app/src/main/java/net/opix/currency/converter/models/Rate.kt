package net.opix.currency.converter.models

import java.io.Serializable

data class Rate(val currency: String, val rate: Double) : Serializable {
}